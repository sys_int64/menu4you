from django.http import HttpResponse, JsonResponse
from django.template import loader
from django.conf import settings
from django.core.mail import send_mail
from django.views.decorators.cache import cache_control
from django.core.exceptions import ValidationError
from django.core.validators import validate_email

from team.views import team_render
from partners.views import partners_render
from reviews.views import last_review_render, reviews_render
from pages.models import *
from settings.models import Settings


def full_page_context(request, title, js_scripts, page_num):


    t_header = loader.get_template('static/header.html')
    t_footer = loader.get_template('static/footer.html')
    t_menu = loader.get_template('static/menu.html')
    t_footer_c = loader.get_template('static/footer_content.html')
    t_feedback = loader.get_template('static/feedback_form.html')

    js_scripts.append("app");

    context = {
        'DEBUG': settings.DEBUG,
        'SITE_NAME': settings.SITE_NAME,
        'PAGE_NAME': title,
        'js_scripts': js_scripts,
        'page_num': page_num,
    }

    if (Settings.objects.all()):
        context['settings'] = Settings.objects.all()[0];

    r_header = t_header.render(context, request)
    r_menu = t_menu.render(context, request)

    context.update(
        {
            "header": r_header,
            "menu": r_menu,
            "last_review": last_review_render(request),
            "reviews": reviews_render(request),
            "partners": partners_render(request),
        }
    )

    r_footer = t_footer.render(context, request)
    r_footer_c = t_footer_c.render(context, request)
    r_feedback = t_feedback.render(context, request)

    context["footer"] = r_footer
    context["footer_content"] = r_footer_c
    context["feedback_form"] = r_feedback

    return context


def base_context(request, title, js_scripts, page_num):


    t_header = loader.get_template('static/header.html')
    t_footer = loader.get_template('static/footer.html')
    t_menu = loader.get_template('static/menu.html')
    t_footer_c = loader.get_template('static/footer_content.html')
    t_feedback = loader.get_template('static/feedback_form.html')

    js_scripts.append("app");

    context = {
        'DEBUG': settings.DEBUG,
        'SITE_NAME': settings.SITE_NAME,
        'PAGE_NAME': title,
        'js_scripts': js_scripts,
        'page_num': page_num,
    }

    if (Settings.objects.all()):
        context['settings'] = Settings.objects.all()[0];

    r_header = t_header.render(context, request)
    r_menu = t_menu.render(context, request)

    context.update(
        {
            "header": r_header,
            "menu": r_menu,
            "last_review": last_review_render(request),
            "reviews": reviews_render(request),
            "partners": partners_render(request),
        }
    )

    r_footer = t_footer.render(context, request)
    r_footer_c = t_footer_c.render(context, request)
    r_feedback = t_feedback.render(context, request)

    context["footer"] = r_footer
    context["footer_content"] = r_footer_c
    context["feedback_form"]  = r_feedback

    return context


# Отправка email

def isNum(data):


    if (data in [None, ""]):
        return False

    try:
        int(data)
    except ValueError:
        return False

    return True


def send_mail_view(request):


    response_data = {}

    name = request.POST.get("name")
    p_code = request.POST.get("phone_code")
    p_num = request.POST.get("phone_number")
    email = request.POST.get("email")
    page = request.POST.get("page")
    form_name = request.POST.get("form_name")

    phone = "+7 (" + str(p_code) + ") " + str(p_num);
    response_data["error"] = False

    if (isNum(page)):
        page = int(page)
    else:
        response_data["error"] = True
        response_data["message"] = "Хакер!"

        return JsonResponse(response_data)

    if (form_name in [None, ""]):
        response_data["error"] = True
        response_data["message"] = "Хакер!"

        return JsonResponse(response_data)

    from_mail = settings.DEFAULT_FROM_EMAIL

    # Validations

    # Name

    if (name in [None, ""]):
        response_data["error"] = True
        response_data["message"] = "Заполните имя"

    # Phone number

    elif (p_code in [None, ""] or p_num in [None, ""]):
        response_data["error"] = True
        response_data["message"] = "Заполните телефон"

    elif (not isNum(p_code) or not isNum(p_num)):
        response_data["error"] = True
        response_data["message"] = "Не верный формат телефона"

    elif (len(p_code) != 3 or len(p_num) != 7):
        response_data["error"] = True
        response_data["message"] = "Не верный формат телефона"

    # Email

    elif (not email in [None, ""]):
        try:
            validate_email(email)
        except ValidationError as e:
            response_data["error"] = True
            response_data["message"] = "Не верный формат email"
        else:
            from_mail = email
    else:
        email = "Не указан"

    if response_data["error"]:
        return JsonResponse(response_data)

    # Send email

    to_mail = {
        1: HomePage.objects.all()[0].email,
        2: MobileManagerProject.objects.all()[0].email,
        3: WaiterTerminalProject.objects.all()[0].email,
        4: TabletMenuProject.objects.all()[0].email,
    }[page]

    subject = "Форма: '" + form_name + "' на '" + settings.SITE_NAME + "'"
    html_body = "<h3>Форма: '" + form_name + "' на \
                '" + settings.SITE_NAME + "'</h3> \
                 <p><strong>Имя:     </strong>" + str(name) + "</p> \
                 <p><strong>E-mail:  </strong>" + str(email) + "</p> \
                 <p><strong>Телефон: </strong>" + str(phone) + "</p>"

    body = "Форма: " + form_name + " \
            Имя: " + str(name) + " \
            E-mail: " + str(email) + " \
            Телефон: " + str(phone)

    code = send_mail(subject, body, from_mail, [to_mail],
                     fail_silently=True, html_message=html_body)

    if code == 0:
        response_data["error"] = True
    else:
        response_data["message"] = "Успех"

    return JsonResponse(response_data)


# Главная страница

@cache_control(public=settings.CACHE_PUBLIC, private=settings.CACHE_PRIVATE,
               must_revalidate=settings.CACHE_MUST_REVALIDATE,
               max_age=settings.CACHE_MAX_AGE)
def index(request):


    t_index = loader.get_template('index.html')
    context = base_context(request, "Главная страница",
                           ["vendors/plugins-scroll", "home"], "1")
    context['team'] = team_render(request)

    if (HomePage.objects.all()):
        context['content'] = HomePage.objects.all()[0];

    return HttpResponse(t_index.render(context, request))


# О проекте

@cache_control(public=settings.CACHE_PUBLIC, private=settings.CACHE_PRIVATE,
               must_revalidate=settings.CACHE_MUST_REVALIDATE,
               max_age=settings.CACHE_MAX_AGE)
def about(request):


    t_about = loader.get_template('about.html')
    context = base_context(request, "О проекте",
                           ["vendors/plugins-scroll", "home"], "1")

    if (AboutPage.objects.all()):
        context['content'] = AboutPage.objects.all()[0];

    return HttpResponse(t_about.render(context, request))


# Мобильный терминал

@cache_control(public=settings.CACHE_PUBLIC, private=settings.CACHE_PRIVATE,
               must_revalidate=settings.CACHE_MUST_REVALIDATE,
               max_age=settings.CACHE_MAX_AGE)
def mobile_terminal(request):


    js_scripts = ["vendors/jquery.slimscroll.min",
                  "vendors/jquery.fullpage.min",
                  "mobile_terminal"]
    t_about = loader.get_template('mobile_terminal.html')
    context = full_page_context(request, "Мобильный терминал", js_scripts, "2")

    if (MobileManagerProject.objects.all()):
        context['content'] = MobileManagerProject.objects.all()[0];

    return HttpResponse(t_about.render(context, request))


# Терминал официанта

@cache_control(public=settings.CACHE_PUBLIC, private=settings.CACHE_PRIVATE,
               must_revalidate=settings.CACHE_MUST_REVALIDATE,
               max_age=settings.CACHE_MAX_AGE)
def waiter_terminal(request):


    js_scripts = ["vendors/jquery.slimscroll.min",
                  "vendors/jquery.fullpage.min",
                  "vendors/slick.min",
                  "waiter_terminal"]
    t_about = loader.get_template('waiter_terminal.html')
    context = full_page_context(request, "Терминал официанта", js_scripts, "3")

    if (WaiterTerminalProject.objects.all()):
        context['content'] = WaiterTerminalProject.objects.all()[0];

    return HttpResponse(t_about.render(context, request))


# Мобильное менб

@cache_control(public=settings.CACHE_PUBLIC, private=settings.CACHE_PRIVATE,
               must_revalidate=settings.CACHE_MUST_REVALIDATE,
               max_age=settings.CACHE_MAX_AGE)
def mobile_menu(request):


    js_scripts = ["vendors/jquery.slimscroll.min",
                  "vendors/jquery.fullpage.min",
                  "vendors/slick.min",
                  "mobile_menu"]
    t_about = loader.get_template('mobile_menu.html')
    context = full_page_context(request, "Мобильное меню", js_scripts, "4")

    if (TabletMenuProject.objects.all()):
        context['content'] = TabletMenuProject.objects.all()[0];

    return HttpResponse(t_about.render(context, request))


# Ошибка 404

def handler404(request):


    t_404 = loader.get_template('404.html')
    context = base_context(request, "Страница не найдена",
                           ["vendors/plugins-scroll", "home"], "1")

    return HttpResponse(t_404.render(context, request))
