"""menu4you URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import handler404
from . import views

handler404 = "menu4you.views.handler404"
urlpatterns = [

    # Home Page
    url(r'^$', views.index),
    url(r'^about/$', views.about),
    url(r'^send-mail/$', views.send_mail_view),
    url(r'^projects/mobile-terminal/$', views.mobile_terminal),
    url(r'^projects/waiter-terminal/$', views.waiter_terminal),
    url(r'^projects/mobile-menu/$', views.mobile_menu),

    # Admin panel
    url(r'^admin/', admin.site.urls),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
