from django.shortcuts import render
from django.template  import loader
from django.conf import settings
from team.models import Employee


def team_render(request):
    team_query = Employee.objects.order_by('order')[:8]
    context = {
        "team": team_query,
    }
    team = loader.get_template('dynamic/team.html')
    return team.render (context, request)
