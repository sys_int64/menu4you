from django.contrib import admin
from team.models import Employee
from adminsortable.admin import SortableAdmin


class TeamAdmin(SortableAdmin):


    list_display  = ('name', 'post')
    search_fields = ['text', 'post']


admin.site.register(Employee, TeamAdmin)
