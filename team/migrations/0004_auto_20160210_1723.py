# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-02-10 17:23
from __future__ import unicode_literals

from django.db import migrations
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('team', '0003_auto_20160117_1244'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employee',
            name='illustration',
            field=sorl.thumbnail.fields.ImageField(blank=True, upload_to='', verbose_name='Иллюстрация'),
        ),
        migrations.AlterField(
            model_name='employee',
            name='photo',
            field=sorl.thumbnail.fields.ImageField(blank=True, upload_to='', verbose_name='Фото'),
        ),
    ]
