# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-17 12:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('team', '0002_auto_20160117_1228'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='employee',
            options={'ordering': ['order'], 'verbose_name': 'Сотрудник', 'verbose_name_plural': 'Команда'},
        ),
        migrations.AddField(
            model_name='employee',
            name='order',
            field=models.PositiveIntegerField(db_index=True, default=0, editable=False),
        ),
    ]
