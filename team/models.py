from django.db import models
from adminsortable.models import SortableMixin
from sorl.thumbnail import ImageField


class Employee(SortableMixin):


    class Meta:
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Команда'
        ordering = ['order']


    name = models.CharField("Имя", max_length=200)
    post = models.CharField("Должность", max_length=200)
    illustration = ImageField("Иллюстрация", blank=True)
    photo = ImageField("Фото", blank=True)
    order = models.PositiveIntegerField(default=0, editable=False,
                                        db_index=True)


    def __str__ (self):
        return self.name
