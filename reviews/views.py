from django.shortcuts import render
from django.template  import loader
from django.conf import settings
from reviews.models import Review


def last_review_render(request):


    review_query = Review.objects.order_by('-date')[:1]
    context = {
        "review": review_query[0]
    }
    review = loader.get_template('dynamic/last_review.html')
    return review.render(context, request)


def reviews_render(request):


    reviews_query = Review.objects.order_by('-date')[1:4]
    context = {
        "reviews": reviews_query
    }
    reviews = loader.get_template('dynamic/reviews.html')
    return reviews.render(context, request)
