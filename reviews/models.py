from django.db import models


class Review(models.Model):


    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'
        ordering = ['-date']


    name = models.CharField("Имя", max_length=200)
    company = models.CharField("Компания", max_length=200)
    date = models.DateField("Дата")
    text = models.TextField("Отзыв")


    def __str__(self):
        return self.name
