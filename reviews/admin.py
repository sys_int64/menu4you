from django.contrib import admin
from reviews.models import Review


class ReviewsAdmin(admin.ModelAdmin):


    list_display = ('name', 'company', 'date')
    search_fields = ['text', 'post']
    list_filter = ['date']


admin.site.register(Review, ReviewsAdmin)
