from django.contrib import admin
from singlemodeladmin import SingleModelAdmin
from settings.models import *
from django.conf import settings


admin.site.site_header = "Администрирование " + settings.SITE_NAME
admin.site.register(Settings, SingleModelAdmin)
