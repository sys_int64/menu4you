from django.db import models


class Settings(models.Model):


    class Meta:
        verbose_name = "Настройки"
        verbose_name_plural = "Настройки"


    phone  = models.CharField("Телефон", max_length=200, blank=True)
    email  = models.CharField("Электронная почта", blank=True, max_length=200)
    vk_url = models.CharField("Ссылка VK", blank=True, max_length=200)
    fb_url = models.CharField("Ссылка Facebook", blank=True, max_length=200)
    tw_url = models.CharField("Ссылка Twitter", blank=True, max_length=200)
    feedback_text = models.TextField ("Акция в форме обратной связи",
                                      blank=True)
