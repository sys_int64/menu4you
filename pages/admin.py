from django.contrib import admin
from singlemodeladmin import SingleModelAdmin
from pages.models import *


# Главная страница

class HomePageAdmin(SingleModelAdmin):


    fieldsets = (
        (None, { 'fields': ('email', 'price', 'video_url',), }),

        ('Стартовый экран', {
            'fields': ('section1_header',
                       'section1_q1',
                       'section1_q2',
                       'section1_q3',
                       'section1_promo',
                       'section1_footer'),
        }),

        ('Мобильный терминал менеджера', {
            'fields': ('section2_l_text',
                       'section2_r_text',
                       'section2_footer'),
        }),

        ('Мобильный терминал официанта', {
            'fields': ('section3_l_text',
                       'section3_r_text',
                       'section3_footer'),
        }),

        ('Наша команда', {
            'fields': ('team_header',
                       'team_text'),
        }),
    )


# Мобильный терминал менеджера

class MobileManagerProjectAdmin(SingleModelAdmin):


    fieldsets = (
        (None, { 'fields': ('email', 'price',), }),

        ('Экран 1', { 'fields': ('screen1_header', 'screen1_text'), }),
        ('Экран 2', { 'fields': ('screen2_header', 'screen2_text'), }),
        ('Экран 3', { 'fields': ('screen3_header', 'screen3_text'), }),
        ('Экран 4', { 'fields': ('screen4_header', 'screen4_text'), }),
    )


# Мобильный терминал официанта

class WaiterSlideInline(admin.StackedInline):


    model   = WaiterSlide
    extra   = 0


class WaiterTerminalProjectAdmin(SingleModelAdmin):


    fieldsets = (
        (None, { 'fields': ('email', 'video_url', 'price',), }),

        ('Экран 1', { 'fields': ('screen1_header' , 'screen1_quote',
                                 'screen1_u_quote', 'screen1_text'), }),
    )

    inlines = [WaiterSlideInline]


# Меню на планшете

class TabletSlideInline(admin.StackedInline):


    model   = TabletSlide
    extra   = 0


class TabletMenuProjectAdmin(SingleModelAdmin):


    fieldsets = (
        (None, { 'fields': ('email', 'video_url', 'price',), }),
    )

    inlines = [TabletSlideInline]


# Регистрация моделей
admin.site.register(HomePage, HomePageAdmin)
admin.site.register(AboutPage, SingleModelAdmin)

# Проекты
admin.site.register(MobileManagerProject, MobileManagerProjectAdmin)
admin.site.register(WaiterTerminalProject, WaiterTerminalProjectAdmin)
admin.site.register(TabletMenuProject, TabletMenuProjectAdmin)
