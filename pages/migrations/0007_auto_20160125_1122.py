# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-25 11:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0006_auto_20160125_0934'),
    ]

    operations = [
        migrations.AddField(
            model_name='homepage',
            name='email',
            field=models.CharField(blank=True, max_length=200, verbose_name='Email для обратной связи'),
        ),
        migrations.AddField(
            model_name='mobilemanagerproject',
            name='email',
            field=models.CharField(blank=True, max_length=200, verbose_name='Email для обратной связи'),
        ),
        migrations.AddField(
            model_name='tabletmenuproject',
            name='email',
            field=models.CharField(blank=True, max_length=200, verbose_name='Email для обратной связи'),
        ),
        migrations.AddField(
            model_name='waiterterminalproject',
            name='email',
            field=models.CharField(blank=True, max_length=200, verbose_name='Email для обратной связи'),
        ),
    ]
