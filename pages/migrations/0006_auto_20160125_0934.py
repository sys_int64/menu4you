# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-25 09:34
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0005_auto_20160125_0904'),
    ]

    operations = [
        migrations.RenameField(
            model_name='aboutpage',
            old_name='section1_footer',
            new_name='goal',
        ),
    ]
