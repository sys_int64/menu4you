from django.db import models
from sorl.thumbnail import ImageField


# Главная страница

class HomePage(models.Model):


    class Meta:
        verbose_name = "Главная"
        verbose_name_plural = "Главная"


    email = models.CharField("Email для обратной связи",
                             max_length=200, blank=True)
    price = models.CharField("Цена", max_length=200, blank=True)
    video_url = models.CharField("Видео", blank=True, max_length=200)

    # Section 1

    section1_header  = models.CharField("Заголовок", max_length=200)
    section1_q1 = models.CharField("Вопрос 1", max_length=200, blank=True)
    section1_q2 = models.CharField("Вопрос 2", max_length=200, blank=True)
    section1_q3 = models.CharField("Вопрос 3", max_length=200, blank=True)
    section1_promo = models.TextField("Текст акции", blank=True)
    section1_footer = models.TextField("Футер", blank=True)

    # Section 2

    section2_l_text = models.TextField("Текст слева", blank=True)
    section2_r_text = models.TextField("Текст справа", blank=True)
    section2_footer = models.TextField("Футер", blank=True)

    # Section 3

    section3_l_text = models.TextField("Текст слева", blank=True)
    section3_r_text = models.TextField("Текст справа", blank=True)
    section3_footer = models.TextField("Футер", blank=True)

    # Our Team

    team_header = models.CharField("Заголовок", max_length=200)
    team_text = models.TextField("Текст", blank=True)


# О проекте

class AboutPage(models.Model):


    class Meta:
        verbose_name = "О проекте"
        verbose_name_plural = "О проекте"


    goal = models.TextField("Наша цель", blank=True)


# Мобильный терминал менеджера

class MobileManagerProject(models.Model):


    class Meta:
        verbose_name = "Мобильный терминал менеджера"
        verbose_name_plural = "Мобильный терминал менеджера"


    # Main
    email = models.CharField("Email для обратной связи",
                             max_length=200, blank=True)
    price = models.CharField("Цена", max_length=200, blank=True)

    # Screen 1
    screen1_header = models.CharField("Заголовок", max_length=200)
    screen1_text = models.TextField("Текст", blank=True)

    # Screen 2
    screen2_header = models.CharField("Заголовок", max_length=200)
    screen2_text = models.TextField("Текст", blank=True)

    # Screen 3
    screen3_header = models.CharField("Заголовок", max_length=200)
    screen3_text = models.TextField("Текст", blank=True)

    # Screen 4
    screen4_header = models.CharField("Заголовок", max_length=200)
    screen4_text = models.TextField("Текст", blank=True)


# Мобильный терминал официанта

class WaiterTerminalProject(models.Model):


    class Meta:
        verbose_name = "Мобильный терминал официанта"
        verbose_name_plural = "Мобильный терминал официанта"


    # Main
    email = models.CharField("Email для обратной связи",
                             max_length=200, blank=True)
    price = models.CharField("Цена", max_length=200, blank=True)
    video_url = models.CharField("Видео", blank=True, max_length=200)

    # Screen 1
    screen1_header = models.CharField("Заголовок", max_length=200)
    screen1_quote = models.TextField("Цитата", blank=True)
    screen1_u_quote = models.TextField("Под цитатой", blank=True)
    screen1_text = models.TextField("Текст", blank=True)


class WaiterSlide(models.Model):


    class Meta:
        verbose_name = "Слайд"
        verbose_name_plural = "Слайды"


    parent = models.ForeignKey(WaiterTerminalProject)
    header = models.CharField("Заголовок", max_length=200)
    text = models.TextField("Текст", blank=True)
    image = ImageField("Изображение", null=True)

# Меню на планшете

class TabletMenuProject(models.Model):


    class Meta:
        verbose_name = "Меню на планшете"
        verbose_name_plural = "Меню на планшете"


    # Main
    email = models.CharField("Email для обратной связи",
                             max_length=200, blank=True)
    price   = models.CharField("Цена", max_length=200, blank=True)
    video_url = models.CharField("Видео", blank=True, max_length=200)


class TabletSlide (models.Model):


    class Meta:
        verbose_name = "Слайд"
        verbose_name_plural = "Слайды"


    parent = models.ForeignKey(TabletMenuProject)
    header = models.CharField("Заголовок", max_length=200)
    text = models.TextField("Текст", blank=True)
    image = ImageField("Изображение")
