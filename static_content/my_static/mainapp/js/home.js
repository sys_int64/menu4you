$(window).scroll(function() {
	var obj = $('.parallax-background');
	var scrolled = $(window).scrollTop();

	$.each($(".parallax-background"), function() {
		var backgroundPos = $(this).css('background-position').split(" ");
		var ratio = parseFloat($(this).attr("parallax-ratio"));
		$(this).css ({transform: "translate3d(0, "+(scrolled * ratio)+"px, -1px)"});
	});

});

$(document).ready(function() {
	try {
		$.browserSelector();

		if ($("html").hasClass("chrome"))
			$.smoothScroll();

	} catch(err) {};
})
