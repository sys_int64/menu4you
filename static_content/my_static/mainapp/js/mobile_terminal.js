
var scrollSpeed = 1000;
var inited = false;
var interval = null;
var lastDirection = "";

function initParallax() {

    $.each($(".parallax"), function() {

        var slide  = $(this).attr("slide");
        var index  = parseInt($(slide).attr("index"));
        var ratio  = parseFloat($(this).attr("parallax-ratio"));
        //var factor = index == 1 ? 0 : 1;
        if (index == 1)
            return;

        var scrolled = -$(slide).height()*factor;
        $(this).css ({transform: "translate3d(0, "+(scrolled * ratio)+"px, -1px)"});

    });

}

function parallax (sign) {

    $.each($(".parallax"), function() {

        var slide = $(this).attr("slide");
        var ratio = parseFloat($(this).attr("parallax-ratio"));
        var scrolled = -$(slide).offset().top+$(slide).height()*sign;
        $(this).css ({transform: "translate3d(0, "+(scrolled * ratio)+"px, -1px)"});

    });

}

function parallaxPhone (nextIndex, direction) {

    var phone_abs = $(".phone-absolute");
    var begin = parseInt($(phone_abs).attr("begin"));
    var end   = parseInt($(phone_abs).attr("end"));

    if (direction == "down" && nextIndex == begin) {
        $(".phone").removeClass ("leave");
    } else

    if (direction == "down" && nextIndex == begin+1) {
        $(".phone").addClass ("hide");
        $(".phone-absolute").removeClass ("hide");
    } else

    if (direction == "up" && nextIndex == 1) {
        $(".phone").addClass ("leave");
        $(".phone-absolute").addClass ("hide");
    } else

    if (direction == "down" && nextIndex == end+1) {
        $(".phone.last").removeClass ("hide");
        $(".phone-absolute").addClass ("hide");
    } else

    if (direction == "up" && nextIndex == end-1) {
        $(".phone.last").addClass ("hide");
        $(".phone-absolute").removeClass ("hide");
    }

}

function parallaxPhoneAfterLoad (index) {

    var phone_abs = $(".phone-absolute");
    var begin = parseInt($(phone_abs).attr("begin"));
    var end   = parseInt($(phone_abs).attr("end"));

    if (lastDirection == "up" && index == begin)
        $(".phone").removeClass ("hide");
}

$(document).ready(function() {

    $('#mobile_terminal').fullpage({
        scrollingSpeed: scrollSpeed,
        scrollOverflow: true,

        afterLoad: function(anchorLink, index) {
            parallaxPhoneAfterLoad (index);
            if (inited) return;
            inited = true;
            initParallax();
        },

        onLeave: function (index, nextIndex, direction) {

            lastDirection = direction;
            parallaxPhone (nextIndex, direction);

            if (direction == "down") parallax (+1);
            if (direction == "up"  ) parallax (-1);

        }
    });

    $("#read-more").click(function(e) {
        $.fn.fullpage.moveSectionDown();
        e.preventDefault();
    });
});
