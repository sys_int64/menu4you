
var scrollSpeed = 1000;
var inited = false;

function initParallax() {

    $.each($(".parallax"), function() {

        var slide  = $(this).attr("slide");
        var index  = parseInt($(slide).attr("index"));
        var ratio  = parseFloat($(this).attr("parallax-ratio"));

        if (index == 1)
            return;

        var scrolled = -$(slide).height();
        $(this).css ({transform: "translate3d(0, "+(scrolled * ratio)+"px, -1px)"});

    });

}

function parallax (sign) {

    $.each($(".parallax"), function() {

        var slide = $(this).attr("slide");
        var ratio = parseFloat($(this).attr("parallax-ratio"));
        var scrolled = -$(slide).offset().top+$(slide).height()*sign;
        $(this).css ({transform: "translate3d(0, "+(scrolled * ratio)+"px, -1px)"});

    });

}

$(document).ready(function() {

    $("#mobile_menu").fullpage({
        scrollingSpeed: scrollSpeed,
        scrollOverflow: true,

        afterLoad: function(anchorLink, index) {
            if (inited) return;
            inited = true;
            initParallax();
        },

        onLeave: function (index, nextIndex, direction) {

            if (direction == "down") parallax (+1);
            if (direction == "up"  ) parallax (-1);

        }
    });

    $("#gallery-slick").slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        autoplay: true,
        autoplaySpeed: 5000,
    });

    $("#gallery-slick").click(function() {
        $("#gallery-slick").slick("slickNext");
    });

    $("#read-more").click(function(e) {
        $.fn.fullpage.moveSectionDown();
        e.preventDefault();
    });

});
