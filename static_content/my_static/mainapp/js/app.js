
function updatePartners() {

	var ps = $(".partner").width();
	$(".partner").height (ps);
	$.each($(".partner .logo"), function() {

		var w = $(this).width();
		var h = $(this).height();

		/*if (w > h) {
			$(this).css ("width" , ps-20);
			$(this).css ("height", "auto");

			w = ps-20;
			h = $(this).height();
		} else {
			$(this).css ("height", ps-20);
			$(this).css ("width" , "auto");

			h = ps-20;
			w = $(this).width();
		}*/

		$(this).css ("width" , ps-20);
		$(this).css ("height", "auto");

		w = ps-20;
		h = $(this).height();

		var x = (ps-w)/2;
		var y = (ps-h)/2;

		$(this).css ("left", x);
		$(this).css ("top" , y);

	});
}

$(document).ready(function() {

	setInterval (function() {
		updatePartners();
	}, 2000);

	$(".feedback-form").submit (function(e) {

		var form    = $(this);
		var results = $(this).find (".results");
		var loading = $(this).find (".loading");
		var message = $(this).find (".message");

		$.ajax({
			url      : "/send-mail/",
			dataType : "json",
			type     : "POST",
			data     :  $(form).serialize(),

			beforeSend : function() {

				$(results).animate ({"height": 50});
				$(loading).animate ({"opacity": 1});
				$(message).animate ({"opacity": 0});

			},

			success: function (data, textStatus) {

				$(loading).animate ({"opacity": 0});
				$(message).animate ({"opacity": 1});

				if (data.error) {
					$(message).addClass ("error");
				} else {
					$(message).removeClass ("error");
					$(form).find("input[type=text], input[type=email], textarea").val("");

					setTimeout(function() {
						$(".modal .close").trigger("click");
					}, 3000);
				}

				$(message).html (data.message);

			},

			error: function() {

				$(loading).animate ({"opacity": 0});
				$(message).animate ({"opacity": 1});
				$(message).addClass ("error");
				$(message).html ("Не удалось выполнить запрос");

			}

		});

		e.preventDefault();

	});

	$(".modal .close").click(function() {

		$(".feedback-form").find("input[type=text], input[type=email], textarea").val("");

		$(".feedback-form .results").animate ({"height" : 0});
		$(".feedback-form .loading").animate ({"opacity": 0});
		$(".feedback-form .message").animate ({"opacity": 0});

	});

	setTimeout(function(){
		$('body').addClass('loaded');
	}, 3000);

});
