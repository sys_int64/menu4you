from django.shortcuts import render
from django.template import loader
from django.conf import settings
from partners.models import Partner


def partners_render(request):


    partners_query = Partner.objects.order_by('order')[:15]
    count = 15-partners_query.count()
    context = {
        "partners": partners_query,
        "count": count,
        "range": range(0, count),
    }
    partners = loader.get_template('dynamic/partners.html')
    return partners.render(context, request)
