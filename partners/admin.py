from django.contrib import admin
from partners.models import Partner
from adminsortable.admin import SortableAdmin


class PartnerAdmin(SortableAdmin):


    list_display  = ('name',)
    search_fields = ['name', 'desc']


admin.site.register(Partner, PartnerAdmin)
