from django.db import models
from adminsortable.models import SortableMixin
from sorl.thumbnail import ImageField


class Partner(SortableMixin):


    class Meta:
        verbose_name = 'Партнер'
        verbose_name_plural = 'Партнеры'
        ordering = ['order']


    name = models.CharField("Название", max_length=200)
    url = models.CharField("Ссылка", max_length=200)
    logo = ImageField("Логотип")
    desc = models.TextField("Описание")
    order = models.PositiveIntegerField(default=0, editable=False,
                                        db_index=True)


    def __str__(self):
        return self.name
